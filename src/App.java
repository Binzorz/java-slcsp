import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import Models.PlanData;
import Models.SlcspData;
import Models.ZipData;

import Services.ZipService;
import Services.PlanService;
import Services.SlcspService;

import java.util.Map;

public class App {
    public static void main(String[] args) throws Exception 
    { 
        ZipService zipService = new ZipService();
        PlanService planService = new PlanService();
        SlcspService slcspService = new SlcspService();
        List<ZipData> zips = zipService.GetProviderZipData();
        List<PlanData> plans = planService.GetProviderPlanData();
        List<SlcspData> slcsps = slcspService.GetProviderSlcspData();

        //Creating Mapped Data grouping by ZIP code.
        Map<Integer, List<ZipData>> zipMap = zips.stream()
        .collect(Collectors.groupingBy(ZipData::getZipCode));

        //Created Mapped Data grouping by State, RateArea, and Metal_Level.
        Map<String, Map<Integer, Map<String, List<PlanData>>>> MSRP = plans.stream()
        .collect(Collectors.groupingBy(PlanData::getState,
        Collectors.groupingBy(PlanData::getRate_area,
        Collectors.groupingBy(PlanData::getMetal_level))));

        List<SlcspData> slcspsOutput = new ArrayList<>();
        // Looping on each provided zip code to identify Second Lowest Cost Silver Plan (SLCSP)
        for (SlcspData slcsp : slcsps){
            //Setting up outputRate here to assume failure for ambiguous zips or no second silver plan
            double outputRate = -1; 
            //Do not bother checking zipcodes with more than 1 State<->Rate Area
            if(zipService.ZipIsNotAmbiguous(zipMap.get(slcsp.getZipCode())))
            {
                String state = zipMap.get(slcsp.getZipCode()).get(0).getState();
                int rateArea = zipMap.get(slcsp.getZipCode()).get(0).getRate_area();
                //Do not bother checking for Plans with no Silver Data.
                outputRate = planService.GetSecondRate(MSRP, state, rateArea);
            }
            SlcspData slcspData = new SlcspData(slcsp.getZipCode(),outputRate);
            slcspsOutput.add(slcspData);
        }
        slcspService.PrintOutput(slcspsOutput);
    }
}
