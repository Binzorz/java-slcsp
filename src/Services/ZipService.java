package Services;

import java.util.ArrayList;
import java.util.List;
import Models.ZipData;
import Providers.ZipProvider;

public class ZipService {

    public List<ZipData> GetProviderZipData() {
        ZipProvider zipProvider = new ZipProvider();
        return zipProvider.readZipDataFromCSV();
    }

    public boolean ZipIsNotAmbiguous(List<ZipData> zipList)
    {
        List<String> stateRate = new ArrayList<String>();
        for(ZipData zip: zipList)
        {
            if(stateRate.indexOf(zip.getState() + zip.getRate_area())==-1)
            {
                stateRate.add(zip.getState() + zip.getRate_area());
            }
        }
        return stateRate.size()==1;
    }
}