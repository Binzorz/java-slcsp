package Services;

import java.text.DecimalFormat;
import java.util.List;
import Models.SlcspData;
import Providers.SlcspProvider;

public class SlcspService {

    private static final DecimalFormat df = new DecimalFormat("0.00");
    
    public List<SlcspData> GetProviderSlcspData() {
        SlcspProvider slcspProvider = new SlcspProvider();
        return slcspProvider.readSlcspDataFromCSV();
    }

    public void PrintOutput(List<SlcspData> slcspsOutput)
    {
        System.out.println("zipcode,rate");
        for(SlcspData slcsp : slcspsOutput)
        {
            if(slcsp.getRate() != -1){
                //Adding in the two decimal place decimal format to our output.
                System.out.println(slcsp.getZipCode() + "," + df.format(slcsp.getRate()));
            }else{
                System.out.println(slcsp.getZipCode() + ",");
            }
        }  
    }
}
