package Services;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Map;
import Models.PlanData;
import Providers.PlanProvider;

public class PlanService {
    
    public List<PlanData> GetProviderPlanData() {
        PlanProvider planProvider = new PlanProvider();
        return planProvider.readPlanDataFromCSV();
    }
    public double GetSecondRate(Map<String, Map<Integer, Map<String, List<PlanData>>>> MSRP, String state, int rateArea)
    {
        double secondRate = -1;
        if(MSRP.containsKey(state) && 
            MSRP.get(state).containsKey(rateArea) && 
            MSRP.get(state).get(rateArea).containsKey("Silver"))
        {
            List<Double> rates = SortPlanRates(MSRP.get(state).get(rateArea).get("Silver"));
            double firstRate = rates.get(0);
            for(double rate : rates)
            {
                if(firstRate != rate)
                {
                    secondRate = rate;
                    break;
                }
            }   
        }
        return secondRate;
    }

    private List<Double> SortPlanRates(List<PlanData> planList)
    {
        List<Double> rates = new ArrayList<Double>();
        for(PlanData plan : planList)
        {
            rates.add(plan.getRate());
        }
        Collections.sort(rates);
        return rates;
    }
}
