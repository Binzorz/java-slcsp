package Models;
// ZipData class houses a mapping of ZIP code to county/counties & rate area(s)
// imported from Zips.csv

public class ZipData {
    int zipCode;
    String state;
    int county_code;
    String name;
    int rate_area;

    public ZipData(int zipCode, String state, int county_code, String name, int rate_area) {
        this.zipCode = zipCode;
        this.state = state;
        this.county_code = county_code;
        this.name = name;
        this.rate_area = rate_area;
    }

    public int getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getCounty_code() {
        return this.county_code;
    }

    public void setCounty_code(int county_code) {
        this.county_code = county_code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRate_area() {
        return this.rate_area;
    }

    public void setRate_area(int rate_area) {
        this.rate_area = rate_area;
    }

    @Override
    public String toString() {
        return "ZipData [zipCode=" + zipCode + ", state= " + state + ", county code= " + county_code + ", county name= " + name + ", rate area= " + rate_area + "]";
    }
}
