package Models;
//Second Lowest Cost Silver Plan(SLCSP) Data class to house the requested zipcodes for
//us to provide rates for.

public class SlcspData {
    int zipCode;
    double rate;

    public SlcspData(int zipCode, double rate) {
        this.zipCode = zipCode;
        this.rate = rate;
    }


    public int getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public double getRate() {
        return this.rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}