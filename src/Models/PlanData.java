package Models;
// PlanData class to house all of the health plans in the U.S. on the marketplace
// imported from plans.csv

public class PlanData 
{
    String plan_id;
    String state;
    String metal_level;
    double rate;
    int rate_area;

    public PlanData(String plan_id, String state, String metal_level, double rate, int rate_area) {
        this.plan_id = plan_id;
        this.state = state;
        this.metal_level = metal_level;
        this.rate = rate;
        this.rate_area = rate_area;
    }

    public String getPlan_id() {
        return this.plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMetal_level() {
        return this.metal_level;
    }

    public void setMetal_level(String metal_level) {
        this.metal_level = metal_level;
    }

    public double getRate() {
        return this.rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getRate_area() {
        return this.rate_area;
    }

    public void setRate_area(int rate_area) {
        this.rate_area = rate_area;
    }

    @Override
    public String toString() {
        return "PlanData [plan_id=" + plan_id + ", state= " + state + ", metal_level= " + metal_level + ", rate= " + rate + ", rate area= " + rate_area + "]";
    }
}