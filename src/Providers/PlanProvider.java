package Providers;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import Models.PlanData;

public class PlanProvider {
    
    public List<PlanData> readPlanDataFromCSV(){
        //TODO: path information should be in a config file, loaded
        // by a config class and dependency injected. For simplicity
        // I'll leave it hardcoded.
        Path currentDir = Paths.get(".");
        Path path = Path.of(currentDir + "\\CSVFiles\\plans.csv");
        List<PlanData> plans = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(path))
        {
            String line = br.readLine();
            line = br.readLine();
        
            while(line != null){
                String[] planVals = line.split(",");
                PlanData plan = createPlanData(planVals);
                plans.add(plan);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return plans;
    }

    private static PlanData createPlanData(String[] strings)
    {
        String plan_id = strings[0];
        String state = strings[1];
        String metal_level = strings[2];
        double rate = Double.parseDouble(strings[3]);
        int rate_area = Integer.parseInt(strings[4]);
        return new PlanData(plan_id, state, metal_level, rate, rate_area);
    }
}
