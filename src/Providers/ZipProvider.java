package Providers;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import Models.ZipData;

public class ZipProvider {

    public List<ZipData> readZipDataFromCSV() {
        //TODO: path information should be in a config file, loaded
        // by a config class and dependency injected. For simplicity
        // I'll leave it hardcoded.
        Path currentDir = Paths.get(".");
        Path path = Path.of(currentDir + "\\CSVFiles\\zips.csv");
        List<ZipData> zipCodeData = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(path))
        {
            String line = br.readLine();
            line = br.readLine();

            while(line != null){
                String[] zipVals = line.split(",");
                ZipData zips = createZipData(zipVals);
                zipCodeData.add(zips);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return zipCodeData;
    }

    private static ZipData createZipData(String[] strings)
    {
        int zipCode = Integer.parseInt(strings[0]);
        String state = strings[1];
        int county_code = Integer.parseInt(strings[2]);
        String name = strings[3];
        int rate_area = Integer.parseInt(strings[4]);
        return new ZipData(zipCode, state, county_code, name, rate_area);
    }
}
