package Providers;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import Models.SlcspData;

public class SlcspProvider {

    public List<SlcspData> readSlcspDataFromCSV(){
        //TODO: path information should be in a config file, loaded
        // by a config class and dependency injected. For simplicity
        // I'll leave it hardcoded.
        Path currentDir = Paths.get(".");
        Path path = Path.of(currentDir + "\\CSVFiles\\slcsp.csv");
        List<SlcspData> slcsps = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(path))
        {
            String line = br.readLine();
            line = br.readLine();
        
            while(line != null){
                String[] slcspVals = line.split(",");
                SlcspData slcsp = createSlcspData(slcspVals);
                slcsps.add(slcsp);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return slcsps;
    }
    
    private static SlcspData createSlcspData(String[] strings)
    {
        int zipCode = Integer.parseInt(strings[0]);
        return new SlcspData(zipCode, -1);
    }
}
