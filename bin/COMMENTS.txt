Steps to run program from a command line:

1) First, you'll need to make sure you have a java JDK installed,
2) and that it has been added to your environment variables path.
3) In a new command line interface, traverse to the projects 'src' folder.
4) Once in the src folder run 'javac App.java'. This should give you no output and a new line.
5) Now run 'java App.java'. The output should print at this point.
